/**
 * @file
 * Javascript functionality for the gather content blocks import module
 */

jQuery(document).ready(function(){
	jQuery(".edit-block-action").change(function(){
		jQuery(this).parent().siblings().children('.block-select').toggleClass('active');
	});
});