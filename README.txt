
-- SUMMARY --

The Gather Content Blocks Import module allows users to import content from
their Gather Content (http://gathercontent.com) account into Drupal blocks.
Users have the ability to create new blocks or override existing blocks.

-- REQUIREMENTS --

* Must have cURL installed on your server.
* Must hav Gather Content Account

-- INSTALLATION/CONFIGURATION --

* Download and install the module
* Enter your gather content account name and API key at http://example.com/admin/config/content/gcblocks
* Choose which project from Gather Content that you would like to make available for imports
* Choose Gather Content pages to make new blocks or override existing ones

-- CONTACT --

Current maintainers:
* Jason D. Jozwiak (jjozwiak) - https://drupal.org/user/1829158

