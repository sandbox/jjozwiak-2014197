<?php

/**
 * @file
 * Contains various utility functions for the gather content blocks import module
 */

function add_new_block($block_info, $block_body, $block_format) {

  //Insert the new block
  $new_block_id = db_insert('block_custom')
    ->fields(array(
      'info' => $block_info,
      'body' => $block_body,
      'format' => $block_format
    ))
    ->execute();

  //Assign settings for this block for each active theme
  foreach (list_themes() as $key => $theme) {
    if ($theme->status) {
      $new_block = db_insert('block')
        ->fields(array(
          'module' => 'block',
          'delta' => $new_block_id,
          'theme' => $theme->name,
          'status' => 0,
          'weight' => 0,
          'custom' => 0,
          'pages' => '',
          'visibility' => 0,
          'title' => '',
          'cache' => -1,
        ))
        ->execute();
    }
  }

}

function update_block($bid, $block_body) {
  $block_update = db_update('block_custom')
    ->fields(array(
      'body' => $block_body
    ))
    ->condition('bid', $bid)
    ->execute();
}

function get_blocks() {
  $result = db_query('SELECT * FROM {block_custom}');
  return $result;
}
