<?php

/**
 * @file
 * Contains admin settings form for gather content blocks import module
 */

require_once('gcapi.php');

function gc_blocks_settings() {
  return drupal_get_form('gc_settings_form');
}

function gc_settings_form() {
  $form = array();
  $form['gc_api_key'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Gather Content API Key'),
    '#default_value' => variable_get('gc_api_key'),
    '#description' => t('You can find your API key by clicking on the "API" tab of the GatherContent settings page.'),
    '#required' => TRUE
  );
  $form['gc_account'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Gather Content Account Name'),
    '#default_value' => variable_get('gc_account'),
    '#description' => t("Your GatherContent account name is the first part of the GatherContent url when you're logged in, eg. https://[accountname].gathercontent.com."),
    '#required' => TRUE
  );
  return system_settings_form($form);
}


/**
 * @todo Try to connect to gather content and return error if connection fails
 */
function gc_settings_form_validate($form, &$form_state) {

  $api_call = new GC_api($form_state['values']['gc_account'], $form_state['values']['gc_api_key']);
  $results = json_decode($api_call->get_me());

  if($results->success == NULL){
    form_set_error('gc_api_key', t('The API key and account name combination provided failed to connect to the Gather Content API. Please double check your credentials.'));
  }

}