<?php

/**
 * @file
 * Contains form to import gahter content data into new blocks or override existing blocks
 * @todo implement help hook for help module
 * @todo add_new_block and update_block should return a success or fail message
 * so we can inform the user if import is success or failure. Also, the message should give a count of blocks updated and overriden
 */

require_once('gcapi.php');
require_once('gcutilities.inc');

function gc_blocks_import() {
  return drupal_get_form('gc_blocks_import_form');
}

function gc_blocks_import_form() {

  $api_call = new GC_api(variable_get('gc_account'), variable_get('gc_api_key'));
  $results = json_decode($api_call->get_pages_by_projectid(variable_get('gc_project')));
  $pages = array();

  foreach ($results->pages as $page) {
    if ($page->name != "Meta") {
      $pages[] = array("page_id" => $page->id, "page_name" => $page->name);
    }
  }

  $results = get_blocks();
  $options = array();
  $options[0] = "Select Block";
  if ($results) {
    while ($row = $results->fetchAssoc()) {
      $options[$row['bid']] = $row['info'];
    }
  }

  //#of pages returned from api call
  $count = count($pages);
  $form = array();

  for ($i = 0; $i<$count; $i++) {

    $form['gc_page'][$i] = array(
      '#type' => 'fieldset',
      '#title' => t("title", array('title' => $pages[$i]['page_name'])),
    );
    $form["gc_page"][$i]["#tree"] = TRUE;
    $form["gc_page"][$i]['import'] = array(
      "#type" => "checkbox",
    );
    $form["gc_page"][$i]['page_id'] = array(
      "#type" => "hidden",
      "#default_value" => $pages[$i]['page_id'],
    );
    $form["gc_page"][$i]['action'] = array(
      "#type" => "select",
      '#title' => t('Action'),
      '#attributes' => array(
        'class' => array('edit-block-action'),
      ),
      '#options' => array(0 => 'Add New', 1 => 'Override Existing'),
    );
    $form["gc_page"][$i]['blocks'] = array(
      "#type" => "select",
      "#title" => t("Select Block"),
      '#attributes' => array(
        'class' => array('block-select'),
      ),
      "#options" => $options,
    );
  }
  $form['#attached']['js'] = array(
    drupal_get_path('module', 'gather_content_blocks_import') . '/gc_blocks.js',
  );
  $form['#attached']['css'] = array(
    drupal_get_path('module', 'gather_content_blocks_import') . '/gc_blocks_style.css',
  );
  $form["submit"] = array(
    "#type" => "submit",
    "#value" => "Submit",
  );

  return $form;
}

function gc_blocks_import_form_submit($form, &$form_state) {

  //loop through the pages we received back from the api call
  foreach ($form_state['values'] as $page) {

    //if the page has been selected for import, import it
    if ($page['import'] === 1) {

      //make api call to get page data
      $api_call = new GC_api(variable_get('gc_account'), variable_get('gc_api_key'));
      $results = json_decode($api_call->get_page_by_id($page['page_id']));

      $block_title = $results->page[0]->name;
      $block_body = "";
      $block_id = $page['blocks'];

      foreach ($results->page[0]->custom_field_values as $value) {
        $block_body .= $value;
      }

      //are we adding a new block or overriding an existing one?
      if ($page['action'] == 0) {
        add_new_block($block_title, $block_body, "full_html");
        $message = 'inserted block';
      }
      else {
        update_block($block_id, $block_body);
        $message = 'updated block';
      }

    }

  }

  drupal_set_message(t("message", array('message' => $message)), 'status', FALSE);
}