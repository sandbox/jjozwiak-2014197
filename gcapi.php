<?php

class GC_api{

        private $account_name;
        private $api_url;
        private $api_key;
        private $password;

        /**
         * Class Constructor
         *
         * @access public
         * @param string $account
         * @param string $api_key
         * @return null
         */

        public function __construct($account, $api_key) {
                $this->account_name = $account;
                $this->api_url = 'https://' . $this->account_name . '.gathercontent.com/api/0.2.1/';
                $this->api_key = $api_key;
                $this->password = 'x'; // leave it as 'x'
        }

        /**
         * Function get_me
         *
         * Retrieves list of projects
         *
         * @access public
         * @return json
         */

        public function get_me() {
          $query = $this->_curl('get_me');
          return $query['response'];
        }

        /**
         * Function get_projects
         *
         * Retrieves list of projects
         *
         * @access public
         * @return json
         */

        public function get_projects() {
          $query = $this->_curl('get_projects');
          return $query['response'];
        }

        /**
         * Function get_pages_by_projectid
         *
         * Retrieves pages within a project
         *
         * @access public
         * @param int $pid
         * @return json
         */

        public function get_pages_by_projectid($pid) {
          $query = $this->_curl('get_pages_by_project', array('id' => $pid));
          return $query['response'];
        }

        /**
         * Function get_page_by_id
         *
         * Retrieves page by id
         *
         * @access public
         * @param int $pid
         * @return json
         */

        public function get_page_by_id($pid) {
          $query = $this->_curl('get_page', array('id' => $pid));
          return $query['response'];
        }

        /**
         * Function _curl
         *
         * Using cURL to access GatherContent API
         *
         * @access private
         * @param string $command
         * @param array $postfields
         * @return array
         */

        private function _curl($command = '', $postfields = array()) {
                $postfields = http_build_query($postfields);
                $session = curl_init();

                curl_setopt($session, CURLOPT_URL, $this->api_url.$command);
                curl_setopt($session, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
                curl_setopt($session, CURLOPT_HEADER, false);
                curl_setopt($session, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/x-www-form-urlencoded'));
                curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($session, CURLOPT_USERPWD, $this->api_key . ":" . $this->password);
                curl_setopt($session, CURLOPT_POST, true);
                curl_setopt($session, CURLOPT_POSTFIELDS, $postfields);

                if (substr($this->api_url, 0, 8) == 'https://') {
                        curl_setopt($session, CURLOPT_SSL_VERIFYPEER, true);
                }

                $response = curl_exec($session);
                $httpcode = curl_getinfo($session, CURLINFO_HTTP_CODE);
                curl_close($session);

                return array( 'code' => $httpcode, 'response' => $response );
        }
}